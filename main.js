const images = [
  "https://images.unsplash.com/photo-1560414239-dcdf7d8d0226?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
  "https://images.unsplash.com/photo-1560419450-b7878dd01a24?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
  "https://images.unsplash.com/photo-1561969451-a51b1034d762?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
  "https://images.unsplash.com/photo-1525002304794-70f63a9a011a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
  "https://images.unsplash.com/photo-1561119331-236e3bcf921c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
  "https://images.unsplash.com/photo-1560720910-9eb473034a49?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
  "https://images.unsplash.com/photo-1538745988171-000787dc77cb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
];

const imgWrapper = document.getElementsByClassName("img-wrapper");
const imgContainer = document.getElementsByClassName("img_container")[0];
const container = document.getElementsByClassName("container");
const prev = document.getElementsByClassName("left");
const next = document.getElementsByClassName("right");
let n = 0;

prev[0].addEventListener("click", e => {
  let moreOffset = container[0].offsetWidth;
  let imgWrapperWidth = imgWrapper[0];

  n++;
  next[0].classList.add("show_button");
  imgWrapperWidth.style.transform = `translateX(${n * moreOffset}px)`;
});

next[0].addEventListener("click", () => {
  let more = container[0].clientWidth;
  let moreOffset = container[0].offsetWidth;
  let imgWrapperWidth = imgWrapper[0];

  //total client width
  let clintWidth = imgContainer.clientWidth;
  imgContainer.style.width = `${clintWidth + more}px`;
  n--;
  imgWrapperWidth.style.transform = `translateX(${n * moreOffset}px)`;
});

const allImages = images.map((item, i) => {
  return `<img class="container" src=${item} alt="imagesArray">`;
});

imgContainer.innerHTML = allImages;
